# **TheHive docker-compose**

# **Configurarea și implementarea Docker**

### Presupunând o instalare proaspătă a Ubuntu 20.04 (testat și pe 22.04)

#### **Instalați Docker și docker-compose**
1. [Install Docker](https://docs.docker.com/get-docker/)
2. [Install Docker Compose](https://docs.docker.com/compose/).
```bash
sudo apt update

sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce

sudo systemctl status docker

sudo curl -L https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Creați grup docker dacă nu există
sudo groupadd docker
# Adăugați utilizatorul în grupul docker
sudo usermod -aG docker $USER
# Rulați următoarea comandă sau Deconectați-vă și conectați-vă din nou și rulați (aceasta nu funcționează, poate fi necesar să reporniți mai întâi mașina)
newgrp docker

# Notă: Dacă comanda docker-compose eșuează după instalare, verificați calea. De asemenea, puteți crea o legătură simbolică către /usr/bin sau orice alt director din calea dvs.
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

### **Preluare, compilare și implementare**
#### Clonati depozitul
```bash
git clone https://gitlab.com/lightcyphers-open/csirt/thehive-project-infra.git ~/thehive
```
#### Intrati în folderul proiectului
```bash
cd ~/thehive
```
#### Copiați .env.example în .env
```bash
cp .env.example .env
```
#### Editați variabilele de mediu
```bash
nano .env
```
#### Asigurați-vă că utilizați cea mai recentă versiune a imaginilor Docker:
```bash
docker-compose pull
```
#### Pentru a porni aplicația
```bash
docker-compose up
```
#### Pentru a rula în fundal
```bash
docker-compose up -d
```
#### Pentru a vizualiza logurile
```bash
docker-compose logs -f
```

### **Pentru a Curăța si Reporni instanța Docker**

#### Puteți opri toate containerele rulând:
```bash
docker-compose stop
```

#### Puteți opri și elimina toate containerele rulând:
```bash
docker-compose down
```
#### Ștergeți containerele
```bash
docker rm -f $(docker ps -a -q)
```
#### Șterge volumele
```bash
docker volume rm $(docker volume ls -q)
```
#### Ridicati containerele
```bash
docker-compose up -d
```

### **Instrucțiuni după pornirea containerelor** 

- **Pasul 1**.  După un timp, TheHive ar trebui să fie funcțional la adresa http://localhost:9000. Datele de autentificare implicite sunt utilizator: 'admin@thehive.local', iar parola: 'secret'.
![Example Image](imgages/1.png)

- **Pasul 2**. Apoi, navigați la http://localhost:9000/administration/platform/cortex pentru a stabili cheia API pentru Cortex.
![Example Image](imgages/2.png)

- **Pasul 3**. Accesați diviziunea Servers și inițiați serverul cortex0. Pagina ar trebui să se deschidă așa cum este arătat în exemplu:
![Example Image](imgages/3.png)

- **Pasul 4**. Este esențial să completați câmpul pentru cheia API. Valoarea cheii API poate fi recuperată accesând http://localhost:9001. Inițial, se va deschide o pagină așa:
![Example Image](imgages/4.png)

- **Pasul 5**. Apoi, făcând clic pe butonul de Actualizare a Bazei de Date (Update Database), va fi afișată pagina următoare:
![Example Image](imgages/5.png)

- **Pasul 6**. Creați un cont nou. După autentificare, veți fi direcționat către următoarea pagină, așa cum se arată în exemplu:
![Example Image](imgages/6.png)

- **Pasul 7**. Accesați organizația Cortex, și următoarea pagină va fi afișată:
![Example Image](imgages/7.png)
Generați o nouă cheie API făcând clic pe butonul de Creare a Cheii API (Create API Key).

- **Pasul 8**. Noua cheie API a fost creată așa cum se arată în exemplul de mai jos:
![Example Image](imgages/9.png) 
Copiați cheia API și introduceți-o în câmpul menționat în **Pasul 4**:
![Example Image](imgages/10.png) 

- **Pasul 9**. Testați conexiunea. Rezultatul ar trebui să prezinte o alertă în partea de jos stângă a ecranului, așa cum se arată în exemplu:
![Example Image](imgages/11.png) 

- **Pasul 10**. Creați bucket-ul cu numele: "thehive" în MinIO. Accesați http://localhost:9090 așa cum se arată în exemplul de mai jos:
![Example Image](imgages/12.png) 

- **Pasul 11**. Introduceți credențialele definite în .env sau numele de utilizator implicit: 'minioadmin' și parola: 'minioadmin'. După autentificare, pagina principală va fi afișată:
![Example Image](imgages/13.png) 

- **Pasul 12**. Creați bucket-ul cu numele: "thehive"
![Example Image](imgages/14.png) 