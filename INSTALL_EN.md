# **TheHive docker-compose**

# **Docker setup and deployment**

### Assuming a fresh Ubuntu 20.04 install (also tested on 22.04)

#### **Install Docker and docker-compose**
1. [Install Docker](https://docs.docker.com/get-docker/)
2. [Install Docker Compose](https://docs.docker.com/compose/).
```bash
sudo apt update

sudo apt install apt-transport-https ca-certificates curl software-properties-common gnupg lsb-release

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt update

sudo apt install docker-ce

sudo systemctl status docker

sudo curl -L https://github.com/docker/compose/releases/download/v2.4.1/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose

# Create docker group if it doesn't exist
sudo groupadd docker
# Add your user to the docker group
sudo usermod -aG docker $USER
# Run the following command or Logout and login again and run (that doesn't work you may need to reboot your machine first)
newgrp docker

# Note: If the command docker-compose fails after installation, check your path. You can also create a symbolic link to /usr/bin or any other directory in your path.
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
```

### **Fetch, compile and deploy**
#### Clone the repo
```bash
git clone https://gitlab.com/lightcyphers-open/csirt/thehive-project-infra.git ~/thehive
```
#### Enter the project folder
```bash
cd ~/thehive
```
#### Copy .env.example into .env
```bash
cp .env.example .env
```
#### Edit environment variables
```bash
nano .env
```
#### Make sure you are using the latest version of the Docker images:
```bash
docker-compose pull
```
#### To start the app up
```bash
docker-compose up
```
#### To run in background
```bash
docker-compose up -d
```
#### To run in background
```bash
docker-compose logs -f
```

### **To Clean Restart of a Docker Instance** 

#### You can stop and remove all containers by running:
```bash
docker-compose down
```
#### Delete containers
```bash
docker rm -f $(docker ps -a -q)
```
#### Delete volumes
```bash
docker volume rm $(docker volume ls -q)
```
#### UP the containers
```bash
docker-compose up -d
```

### **Instructions after starting the containers** 

- **Step 1**. After some time, TheHive should be up and running at http://localhost:9000. The default login credentials are username: 'admin@thehive.local', and password: 'secret'.
![Example Image](imgages/1.png)

- **Step 2**. Next, navigate to http://localhost:9000/administration/platform/cortex to establish the API Key for Cortex.
![Example Image](imgages/2.png)

- **Step 3**. Proceed to the Servers division and initiate the cortex0 server. The page should open as shown in the example:
![Example Image](imgages/3.png)

- **Step 4**. It's essential to complete the API key field. The API key's value can be retrieved by accessing http://localhost:9001. Initially, a page like this will open:
![Example Image](imgages/4.png)

- **Step 5**. Then, by clicking on the Update Database button, the subsequent page will be displayed:
![Example Image](imgages/5.png)

- **Step 6**. Create a new account. After logging in, you'll be directed to the next page as shown in the example:
![Example Image](imgages/6.png)

- **Step 7**. Access the Cortex organization, and the following page will be displayed:
![Example Image](imgages/7.png)
Generate a new API key by clicking on the Create API Key button.

- **Step 8**. The new API Key has been created as shown in the example below:
![Example Image](imgages/9.png) 
Copy the API key and input it into the field mentioned in **Step 4**:
![Example Image](imgages/10.png) 

- **Step 9**. Test the connection. The result should present an alert at the bottom left of the screen, as shown in the example:
![Example Image](imgages/11.png) 

- **Step 10**. Establish the "thehive" bucket in MinIO. Access http://localhost:9090 as shown in the example below:
![Example Image](imgages/12.png) 

- **Step 11**. Enter the credentials defined in .env or the default username: 'minioadmin' and password: 'minioadmin'. After logging in, the main page will be displayed:
![Example Image](imgages/13.png) 

- **Step 12**. Create the bucket named: "thehive"
![Example Image](imgages/14.png) 